# coding utf-8

import numpy as np
from glob import glob
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split


# Define o range de pacientes com leucemia
y1 = np.arange(1, 131)
# Define o range de pacientes sem leucemia
y0 = np.arange(131, 261)

# Separa os pacientes em 30% para teste
y1_train, y1_test = train_test_split(y1, test_size=0.3, random_state=42)
y0_train, y0_test = train_test_split(y0, test_size=0.3, random_state=42)

# Junta os pacienetes em cada conjunto
train_index = shuffle(np.hstack([y1_train, y0_train]), random_state=42)
test_index = shuffle(np.hstack([y1_test, y0_test]), random_state=42)

# Escreve um arquivo com os diretórios das imagens de treino
fout = open("train.txt", "w")
for i in train_index:
    examples = glob(("Trabalho/Im%3i*" % (i)).replace(" ", "0"))
    for f in examples:
        fout.write("%s\n" % f)
fout.close()

# Escreve um arquivo com os diretórios das imagens de teste
fout = open("test.txt", "w")
for i in test_index:
    examples = glob(("Trabalho/Im%3i*" % (i)).replace(" ", "0"))
    for f in examples:
        fout.write("%s\n" % f)
fout.close()