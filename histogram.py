# coding: utf-8

import sys
import numpy as np
import cv2
import glob

def make_hist(dataset="train"):
    path_images = open("%s.txt" % dataset, "r").readlines()
    
    # Abre os arquivos de saída
    blue = []
    red = []
    green = []
    rgb = []
    pb = []
    labels = []
    
    for archive in path_images:
        path_image = archive.replace("\n", "")
        labels.append(int(path_image[-5]))
 
        # Abre a imagem
        img = cv2.imread(path_image)

        # Calcula o numero de pixels
        res = img.shape[0] * img.shape[1]
        
        # Calcula o histograma RGB
        hist, _ = np.histogram(img.flatten(), 256, [0,256])
        rgb.append(hist / (res * 3))
      
        # Converte para Preto e Branco e calcula o histograma
        hist, _  = np.histogram(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY), 256, [0,256])
        pb.append(hist / res)

        # Separa os canais RGB em B, G e R
        (b, g, r) = cv2.split(img)
        # Calcula o histograma do canal B
        hist, _  = np.histogram(b, 256, [0,256])
        blue.append(hist / res)

        # Calcula o histograma do canal G
        hist, _  = np.histogram(g, 256, [0,256])
        green.append(hist / res)
 
        # Calcula o histograma do canal R
        hist, _  = np.histogram(r, 256, [0,256])
        red.append(hist / res)
    
    # Escreve o histograma preto e branco
    fout = open("data/%s_hist_pb.npy" % dataset, "wb")
    np.save(fout, np.array(pb))
    fout.close()

    # Escreve o histograma do componente B
    fout = open("data/%s_hist_blue.npy" % dataset, "wb")
    np.save(fout, np.array(blue))
    fout.close()

    # Escreve o histograma do componente R
    fout = open("data/%s_hist_red.npy" % dataset, "wb")
    np.save(fout, np.array(red))
    fout.close()

    # Escreve o histograma do componente G
    fout = open("data/%s_hist_green.npy" % dataset, "wb")
    np.save(fout, np.array(green))
    fout.close()

    # Escreve o histograma RGB
    fout = open("data/%s_hist_rgb.npy" % dataset, "wb")
    np.save(fout, np.array(rgb))
    fout.close()

    # Escreve os labels
    fout = open("data/%s_labels.npy" % dataset, "wb")
    np.save(fout, np.array(labels))
    fout.close()

if __name__ == "__main__":
    # salva os histogramas para o  conjunto de treinamento
    make_hist()

    # salva os histogramas para o  conjunto de teste
    make_hist("test")
