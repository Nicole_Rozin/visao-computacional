ETAPAS DO TRABALHO:

** A pasta Trabalho com as imagens deve ser adicionada.

1. Separação do conjunto de treinamento e teste

- Script: split_dataset.py
- Saída: Salva os arquivos train.txt e teste.txt com o caminho das imagens
- Execução: 
>>> python split_dataset.py

2. Extração de características (histograma):
- Script: histogram.py
- Saída: salva 6 arquivos por conjunto de treinamento (train e test):
    - <conjunto de treinamento>_hist_pb.npy -- Histograma P e B
    - <conjunto de treinamento>_hist_blue.npy -- Histograma canal B
    - <conjunto de treinamento>_hist_green.npy -- Histograma canal G
    - <conjunto de treinamento>_hist_red.npy -- Histograma canal R
    - <conjunto de treinamento>_hist_rgb.npy -- Histograma RGB
    - <conjunto de treinamento>_labels.npy -- Classificação dos pacientes
- Execução:
>>> python histohram.py
    
3. Prametrização dos modelos
- Script: classification_model.py
- Saída: Arquivo com o dicionário do resultado da busca em grade (prts_<nome do metodo>)
- Execução:
>>> python classification_model.py -t 0 -ht pb -m <metodo: knn ou random_forest>

Obs.: -t 0 indica que a tarefa realizada é de busca em grade -ht pb indica que po histograma usado é pb

4. Teste dos modelos:
- Script: classification_model.py
- Saída: imprime no terminal o relatório de classificação e a matriz de confusão
- Execução:
>>> python classification_model.py -t 1 -ht <tipo do histograma> -m <metodo de classificação>

Obs.: -t 0 indica que a tarefa realizada é de teste; Os valores de -ht utilizados são: pb, blue, red, green, rgb, ou seja, os mesmos utilizados no nome do arquivo de saída da etapa 2. E, por último, -m aceita os dois métodos utilizados: knn e random_forest.
    
Modelo conjunto:
    Não foi implementada nenhuma função para isso, mas utilizou-se a classe Model no script calssification_model para obter as previsão dos dois modelos com configuração (Método, Histograma) iguais à (random_forest, red), (random_forest, pb) e (knn, red) e depois verificado o voto majoritário, calculando-se a média e arredondando para o inteiro mais próximo: 0 ou 1.
