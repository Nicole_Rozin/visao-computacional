#!/usr/bin/python
# -*- encoding: iso-8859-1 -*-

import argparse
import pickle
import numpy as np
from sklearn.datasets import load_svmlight_file
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import GridSearchCV


PRTS = {"random_forest": {"random_state": [42],
                          "n_estimators": [10, 20, 50, 100, 150, 200],
                          "criterion": ['gini', 'entropy'],
                          "max_depth": [5, 7, 9, 10, 15, 20, 25, 40, None],
                          "max_features": ['sqrt', 'auto', 'log2']},
        "knn": {"n_neighbors": [1,3,5,7,11,13,17],
                "weights": ['uniform', 'distance'],
                "algorithm": ['auto', 'ball_tree', 'kd_tree'],
                "metric": ["euclidean", "manhattan"]},
             }

METHODS = {"random_forest": RandomForestClassifier,
           "knn": KNeighborsClassifier}


class Model:
    def __init__(self, input_file):
        self.read_data(input_file)

    def read_data(self, htype):
        #X_train, self.y_train = load_svmlight_file(filename)
        #X_test, self.y_test = load_svmlight_file(filename.replace("train", "test"))
        f = open("data/train_hist_%s.npy" % (htype), "rb")
        self.X_train = np.load(f)
        f.close()

        f = open("data/test_hist_%s.npy" % (htype), "rb")
        self.X_test = np.load(f)
        f.close()

        f = open("data/train_labels.npy", "rb")
        self.y_train = np.load(f)
        f.close()

        f = open("data/test_labels.npy", "rb")
        self.y_test = np.load(f)
        f.close()
        
        #self.X_train = X_train.toarray()
        #self.X_test = X_test.toarray()

    def search_model(self, method, cv):
        # Executa a busca em grade
        gs_clf = GridSearchCV(METHODS[method](), PRTS[method],
                              cv=cv, n_jobs=-1)
        gs_clf.fit(self.X_train, self.y_train)

        # Salva os resultados e os melhores parametros
        result = {"best": gs_clf.best_params_, "results": gs_clf.cv_results_}
        print(result["best"])

        f = open("data/prts_%s" % (method), "wb")
        pickle.dump(result, f)
        f.close()

    def train_model(self, method):
        # Lê o resultado da busca em grade
        f = open("data/prts_%s" % (method), "rb")
        result = pickle.load(f)
        f.close()

        # Treina o modelo
        clf = METHODS[method](**result["best"])
        clf.fit(self.X_train, self.y_train)
        return clf

    def test_predict(self, method):
        clf = self.train_model(method)
        # Testa o modelo
        y_pred = clf.predict(self.X_test)

        # Gera o reporte de classificação
        cr = classification_report(self.y_test, y_pred)

        # Gera a matriz de confusão
        cm = confusion_matrix(self.y_test, y_pred)

        #print(accuracy, precision, f1score, recall, cr, cm)

        return cm, cr, y_pred
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-ht", "--hist_type", type=str,
                        help='histogram type',
                        choices=["blue", "red", "green", "rgb", "pb"],
                        required=True)

    parser.add_argument("-t", "--task", type=int,
                        help='0 - grid_search or 1 - test',
                        choices=[0, 1],
                        default=0)

    parser.add_argument("-m", "--method", type=str,
                        help='Classification method',
                        choices=["random_forest", "knn"],
                        required=True)

    args = parser.parse_args()

    ml = Model(args.hist_type)

    if args.task == 1:
        matrix, report, y_pred = ml.test_predict(args.method)

        print('Report:\n', report)
        print('Confusion Matrix:\n', matrix)

    else:
        ml.search_model(args.method, 5)
